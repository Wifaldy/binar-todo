const app = require('./app')
const http = require('http')
require("dotenv").config()
const port = process.env.PORT

const server = http.createServer(app)

server.listen(port, () => {
    console.log(`App listening on port ${process.env.PORT}`)
})