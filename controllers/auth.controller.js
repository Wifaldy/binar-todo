const { User } = require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

class AuthController {
  static async login(req, res, next) {
    try {
      const user = await User.findOne({
        where: {
          email: req.body.email
        }
      })

      if (!user) {
        throw {
          status: 401,
          message: 'Invalid email or password'
        }
      }
      if (bcrypt.compareSync(req.body.password, user.password)) {
        const token = jwt.sign({
          id: user.id,
          email: user.email
        }, 'qweqwe')

        res.status(200).json({
          token
        })
      } else {
        throw {
          status: 401,
          message: 'Invalid email or password'
        }
      }
    } catch (err) {
      next(err)
    }
  }
}

module.exports = AuthController